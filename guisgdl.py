from tkinter import *
from tkinter import Label as Lab
from tkinter import Entry as Ent
from tkinter import messagebox
from tkinter.ttk import *
from tkinter.scrolledtext import ScrolledText
from tkinter.filedialog import askopenfilename, askopenfilenames, asksaveasfilename
from PIL import Image, ImageTk, ImageDraw
from math import ceil
from time import time, sleep
from multiprocessing import Process, Queue, freeze_support, Value

class GUI(Tk):
    def __init__(self):
        super().__init__()
        self.title("SGDL Editor")
        self.text = ScrolledText(self, relief = FLAT)
        self.top = Toplevel(self)
        self.label = Lab(self.top,bd=0,highlightthickness=0)
        self.frame = Frame(self)
        self.menu = Menu(self)
        self.left_b = Button(self.frame, command = self.left, text = "<")
        self.right_b = Button(self.frame, command = self.right, text = ">")
        self.mode_b = Button(self.frame, command = self.mode, text = "Mode")
        self.check_time = 0
        self.check_path = ""
        self.changed = False# cursed name
        self.stopper_value = Value("i", 1)
        self.q_in, self.q_out = Queue(), Queue()
        self.eng = Process(target = engine, args = (self.q_in, self.q_out, self.stopper_value))
        self.eng.start()
        self.keys = []
        self.the_dict = dict()

        self.filemenu = Menu(self.menu, tearoff=0)
        self.filemenu.add_command(label = "Image to SGDL", command = self.load)
        self.filemenu.add_command(label = "Save as image", command = self.save)
        self.filemenu.add_command(label = "Load SGDL", command = self.load_sgml)
        self.filemenu.add_command(label = "Save SGDL", command = self.save_sgml)
        self.menu.add_cascade(label = "File", menu = self.filemenu)
        self.checkmenu = Menu(self.menu, tearoff=0)
        self.checkmenu.add_command(label = "Configure checkpoint", command = self.set_check)
        self.menu.add_cascade(label = "Checkpoint", menu = self.checkmenu)
        self.editmenu = Menu(self.menu, tearoff=0)
        self.editmenu.add_command(label = "Reformat", command = self.simplify)
        #Do NOT uncomment this one#self.editmenu.add_command(label = "As spreadsheet", command = self.spread)
        self.menu.add_cascade(label = "Edit", menu = self.editmenu)

        self.config(menu = self.menu)
        self.frame.pack(expand = False, fill = X, side = BOTTOM)
        self.text.pack(expand = True, fill = BOTH, side = BOTTOM)
        self.left_b.pack(side = LEFT)
        self.right_b.pack(side = LEFT)
        self.mode_b.pack(side = LEFT)
        self.label.pack(fill = BOTH, expand = True)
        self.top.minsize(300,300)
        self.focus = 0
        self.frameshift = time()
        self.animate = True
        self.hightime = time()
        self.key = False

        self.after(10, self.upd)
        self.text.bind("<KeyPress>", lambda e: self.after(10,self.on_key(e)))#lambda e: self.after(10, self.highlight))
        self.text.bind("<Control-r>", self.replace)
        self.label.bind("<Left>", self.left)
        self.label.bind("<Right>", self.right)
        self.check()
        self.title("SGDL Editor Code")
        self.top.title("SGDL Editor Viewer")
        self.top.protocol("WM_DELETE_WINDOW", self.reappear)
        self.mainloop()

    def reappear(self, event = None):
        messagebox.showwarning("Warning", "You can only close this window by closing the main window and thus exiting the program." )

    def spread(self):
        self.simplify()
        text_original = remove_comments(self.text.get("1.0",END))
        text = purify(remove_comments(self.text.get("1.0",END).replace("\n", " ")))
        the_dict = dict([(text.split("#")[n].split(" ",1)) for n in range(1,len(text.split("#")),1)])
        if not "sgdl" in list(the_dict.keys()):
            return None
        keys = list(the_dict.keys())
        if "background" in keys:
            bg = eval(the_dict["background"])
        else:
            bg = (0,0,0,0)
        palette = dict([(i.split(":")[0],getcolor(i.split(":")[1])) for i in the_dict["colors"].split(" ")[:-1] if i.replace(" ","")])
        palette["_"] = (0,0,0,0)
        palkeys = list(palette.keys())
        palkeys.sort(key = lambda element: -len(element))
        for pkey in palkeys:
            self.text.tag_delete(pkey)
        if "layers" in keys:
            tags = get_layers(the_dict["layers"])
        else:
            tags = ["pixels"]
            
        inp = Input(self, [("Layer: ", tags),"X: ", "Y: "])
        spr = SpreadSheet(self, self.input[0], self.input[1], self.input[2])

    def on_key(self, e=None):
        self.key = True
        self.changed = True
        if time()-self.hightime>0.1:
            self.hightime = time()
        else:
            self.update()

    def simplify(self):
        text = text = remove_comments(self.text.get("1.0",END).replace("\n", " "))
        the_dict = dict([(text.split("#")[n].split(" ",1)) for n in range(1,len(text.split("#")),1)])
        keys = list(the_dict.keys())
        w = eval(the_dict["width"])
        palette = dict([(i.split(":")[0],getcolor(i.split(":")[1])) for i in the_dict["colors"].split(" ")[:-1] if i.replace(" ","")])
        palette["_"] = (0,0,0,0)
        palette["<"] = 0
        palette["="] = 1
        palette[">"] = 2
        palette["."] = 4
        palkeys = list(palette.keys())
        if "layers" in keys:
            tags = get_layers(the_dict["layers"])
        else:
            tags = ["pixels"]
        for tag in tags:
            count = 0
            focus = 0
            content = the_dict[tag]
            new = ""
            text = content
            while "(" in text:
                content = text[text.find("(")+1:text.find(")")]
                end = start = text.find("*")+1
                while end<len(text) and text[end] in " 0123456789":
                    end+=1
                text = text.replace(text[text.find("("):end],content*eval(text[start:end]),1)
            content = text
            while focus<len(content):
                if 0 in [content[focus:].find(str(i)) for i in palkeys]:
                    new+=palkeys[[content[focus:].find(str(i)) for i in palkeys].index(0)]
                    count+=1
                    if count==w or content[focus]==".":
                        new+="\n"
                        count = 0
                    focus+=len(palkeys[[content[focus:].find(str(i)) for i in palkeys].index(0)])
                else:
                    focus+=1
            the_dict[tag] = "\n"+new
        newtext = "\n".join([f"#{key} {the_dict[key]}" for key in list(the_dict.keys())])
        self.text.delete("1.0",END)
        self.text.insert("1.0",newtext)

    def replace(self, e=None):
        Input(self,["Search for:","Replace with:"])
        one, two = [str(i) for i in self.input]
        try:
            selection = self.text.get("sel.first","sel.last")
            if not selection:
                selection = self.text.get("1.0",END)
                focus = 0
            else:
                focus = self.text.count("1.0", "sel.first")[0]
            new = self.text.get("1.0",END)[:focus]+self.text.get("1.0",END)[focus:focus+len(selection)].replace(one,two)+self.text.get("1.0",END)[focus+len(selection):]
            self.text.delete("1.0",END)
            self.text.insert("1.0",new)
            self.highlight()
        except Exception:
            messagebox.showerror("Replacement Error", "Select text before applying replacement!")

    def set_check(self):
        Input(self,["Checkpoint name:","Autosave interval:"])
        name, interval = self.input
        self.check_path = name
        self.check_time = round(interval*1000)

    def check(self):
        if self.check_time and self.check_path:
            if self.check_path.endswith(".sgdl.chk"):
                pass
            elif self.check_path.endswith(".sgdl"):
                self.check_path+=".chk"
            else:
                self.check_path+=".sgdl.chk"
            with open(self.check_path, "w") as f:
                f.write(self.text.get("1.0",END))
            self.after(self.check_time, self.check)
        else:
            self.after(5000, self.check)
        
    def mode(self):
        self.animate = not self.animate

    def highlight(self, e=None):
        self.text.tag_delete(ALL)
        text_original = remove_comments(self.text.get("1.0",END))
        text = purify(remove_comments(self.text.get("1.0",END).replace("\n", " ")))
        the_dict = dict([(text.split("#")[n].split(" ",1)) for n in range(1,len(text.split("#")),1)])
        if not "sgdl" in list(the_dict.keys()):
            return None
        keys = list(the_dict.keys())
        if "background" in keys:
            bg = eval(the_dict["background"])
        else:
            bg = (0,0,0,0)
        palette = dict([(i.split(":")[0],getcolor(i.split(":")[1])) for i in the_dict["colors"].split(" ")[:-1] if i.replace(" ","")])
        palette["_"] = (0,0,0,0)
        palkeys = list(palette.keys())
        palkeys.sort(key = lambda element: -len(element))
        for pkey in palkeys:
            self.text.tag_delete(pkey)
        if "layers" in keys:
            tags = get_layers(the_dict["layers"])
        else:
            tags = ["pixels"]
        for tag in tags:
            start = text.find("#"+tag)+len(tag)+1
            end = start+1
            while end<len(text):
                if text[end]=="#":
                    break
                else:
                    end+=1
            for pkey in palkeys:
                focus = start
                while pkey in text[focus:] and focus<end:
                    if pkey in [">","=","<","-","+"]:
                        focus+=1
                        continue
                    found = text.find(pkey, focus)
                    y = text_original[:found].count("\n")+1
                    x = found - len("\n".join(text_original.split("\n")[:y-1]))
                    if not "\n".join(text_original.split("\n")[:y-1]):
                        x+=1
                    if not_tag(text, found):
                        self.text.tag_add(pkey, f"{y}.{x-1}", f"{y}.{x+len(pkey)-1}")
                    focus = found+1
                bg = hexcolor(palette[pkey])
                fg = "#000000" if sum(palette[pkey][:-1])>3*255/2 else "#ffffff"
                self.text.tag_config(pkey, background=bg, foreground=fg)

    def upd(self):
        if time()-self.hightime>0.25 and self.key:
            try:
                self.highlight()
            except Exception:
                pass
            self.hightime = time()
            self.key = False
        try:
            if self.changed or (self.animate and "layers" in self.keys and "duration" in self.keys and time()-self.frameshift>eval(self.the_dict["duration"])/1000):
                if self.q_in.empty():
                    self.q_in.put(self.text.get("1.0",END))
                self.changed = False
            if not self.q_out.empty():
                while not self.q_out.empty():
                    self.image = self.q_out.get()
                #self.image = from_sgml(self.text.get("1.0",END))
                self.length = len(self.image[0]) if isinstance(self.image, tuple) else 1
                if isinstance(self.image, tuple):
                    self.images = self.image[0]
                    self.dur = self.image[-1]
                    self.image = self.image[0][self.focus]
                else:
                    self.dur = 0
                    self.images = [self.image]
                if self.image:
                    self.tkimage = ImageTk.PhotoImage(self.image)
                    self.label.config(image = self.tkimage)

                if self.animate:
                    text_original = remove_comments(self.text.get("1.0",END))
                    text = purify(remove_comments(self.text.get("1.0",END).replace("\n", " ")))
                    the_dict = dict([(text.split("#")[n].split(" ",1)) for n in range(1,len(text.split("#")),1)])
                    keys = list(the_dict.keys())
                    self.keys = keys
                    self.the_dict = the_dict
                    if "layers" in keys:
                        tags = get_layers(the_dict["layers"])
                        if "duration" in keys and time()-self.frameshift>eval(the_dict["duration"])/1000:
                            self.focus = (self.focus+1)%len(tags)
                            self.frameshift = time()
        except Exception as e:
            pass
        try:
            self.text.tag_add("sel", "sel.first", "sel.last")
            self.text.tag_config("sel", background="#99D9EA", foreground="#000000")
            self.text.tag_raise("sel")
        except Exception:
            pass
        self.after(10, self.upd)

    def load(self):
        text = make_sgml(askopenfilenames())
        self.text.delete("1.0",END)
        self.text.insert("1.0", text)
        self.key = self.changed = True
        self.keys = []
        self.the_dict = dict()

    def load_sgml(self):
        f = open(askopenfilename(), encoding="utf-8")
        text = f.read()
        f.close()
        self.text.delete("1.0",END)
        self.text.insert("1.0", text)
        self.key = self.changed = True
        self.keys = []
        self.the_dict = dict()

    def save(self):
        fname = asksaveasfilename(filetypes = [("Common Lossless Graphics", "*.png"),
                                               ("Legacy Lossless Graphics", "*.bmp"),
                                               ("Animated Graphics", "*.gif")],
                                  defaultextension = [("Common Lossless Graphics", "*.png"),
                                               ("Legacy Lossless Graphics", "*.bmp"),
                                               ("Animated Graphics", "*.gif")])
        if not True in [ext in fname for ext in (".png",".bmp",".gif")]:
            fname+=".gif"
        if self.dur:
            self.images[0].save(fname, save_all=True, append_images=self.images[1:], loop=0, duration = self.dur)
        else:
            self.images[0].save(fname)

    def save_sgml(self):
        fname = asksaveasfilename(filetypes = [("Simple Graphical Description Language File", "*.sgdl"),
                                               ("Text File", "*.txt"),
                                               ("Animated Graphics", "*.gif")])
        if not True in [ext in fname for ext in (".sgdl",".txt")]:
            fname+=".sgdl"
        with open(fname, mode="w") as f:
            f.write(self.text.get("1.0", END))

    def left(self):
        self.focus = self.focus-1 if self.focus else 0
        if self.q_in.empty():
            self.q_in.put(self.text.get("1.0",END))
        self.changed = False

    def right(self):
        self.focus = (self.focus+1)%self.length if self.focus < self.length else self.length-1
        if self.q_in.empty():
            self.q_in.put(self.text.get("1.0",END))
        self.changed = False

class Input(Toplevel):
    def __init__(self, master, labels, **kwargs):
        super().__init__(**kwargs)
        frames = []
        self.master = master
        self.variables = []
        self.labels_ = []
        self.entries = []
        self.master.input = []
        frames.append(Frame(self))
        for num, label in enumerate(labels):
            if isinstance(label,str):
                self.labels_.append(Label(frames[-1], text = label))#, font = "Callibri 10"))
                self.variables.append(StringVar())
                self.entries.append(Entry(frames[-1], textvariable = self.variables[-1]))#, font = "Callibri 12"))

                
            else:
                label, value = label
                self.labels_.append(Label(frames[-1], text = label))#, font = "Callibri 10"))
                self.variables.append(StringVar())
                self.entries.append(Combobox(frames[-1], textvariable = self.variables[-1], values = value))#, font = "Callibri 12"))

            self.labels_[-1].grid(column = 0, row = num)
            self.entries[-1].grid(column = 1, row = num, padx = (0,10), sticky = EW)
        frames[-1].pack(expand = True, fill = BOTH)
        frames[-1].columnconfigure(1, weight = 1)

        okay = Button(self, text = "Ok", command = self.onpress)
        okay.pack()
        self.mainloop()

    def onpress(self):
        self.master.input = [evaluate(i.get()) for i in self.variables]
        self.quit()
        self.destroy()

class SpreadSheet(Toplevel):#SpreadSheet(self, "pixels", 10, 10)
    def __init__(self, master, layer, x, y):
        super().__init__()
        self.master = master
        self.layer = layer
        self.variables = []
        self.cells = []
        for row in range(y):
            self.rowconfigure(row, weight = 1)
            for column in range(x):
                self.columnconfigure(column, weight = 1)
                self.variables.append(StringVar())
                self.cells.append(Ent(self, width = 3, textvariable = self.variables[-1]))
                self.cells[-1].grid(row = row, column = column, sticky = "wens")
                
        text_original = remove_comments(self.master.text.get("1.0",END))
        text = purify(remove_comments(self.master.text.get("1.0",END).replace("\n", " ")))
        the_dict = dict([(text.split("#")[n].split(" ",1)) for n in range(1,len(text.split("#")),1)])
        if not "sgdl" in list(the_dict.keys()):
            return None
        keys = list(the_dict.keys())
        if "background" in keys:
            bg = eval(the_dict["background"])
        else:
            bg = (0,0,0,0)
        palette = dict([(i.split(":")[0],getcolor(i.split(":")[1])) for i in the_dict["colors"].split(" ")[:-1] if i.replace(" ","")])
        palette["_"] = (0,0,0,0)
        palkeys = list(palette.keys())
        palkeys.sort(key = lambda element: -len(element))
        self.palette = dict()
        self.palkeys = palkeys
        for key in palkeys:
            self.palette[key] = (hexcolor(palette[key]), "#000000" if sum(palette[key][:-1])>3*255/2 else "#ffffff")

        pixels = the_dict[layer]
        values = []
        while pixels:
            if 0 in [pixels.find(pkey) for pkey in palkeys]:
                v = palkeys[[pixels.find(pkey) for pkey in palkeys].index(0)]
                values.append(v)
                pixels = pixels[len(v):]
            else:
                pixels = pixels[1:]
        for n in range(len(self.variables)):
            if values:
                self.variables[n].set(values.pop(0))
        self.after(100, self.highlight)

    def highlight(self):
        for n in range(len(self.cells)):
            color = self.variables[n].get()
            if color in self.palkeys:
                bg, fg = self.palette[color]
                self.cells[n].config(fg = fg, bg = bg)
        self.after(100, self.highlight)
        

def engine(q_in, q_out, stopper):
    while stopper.value:
        if not q_in.empty():
            while not q_in.empty():
                text = q_in.get()
            try:
                result = from_sgml(text)
                q_out.put(result)
            except Exception:
                sleep(1/100)
        else:
            sleep(1/100)

def evaluate(variable):
    try:
        out = eval(variable)
    except (NameError, SyntaxError):
        out = variable
    return out

def not_tag(text, found):
    while found>0:
        found-=1
        if text[found]==" ":
            return True
        elif text[found]=="#":
            return False
    return False

def purify(text):
    textlist = text.replace("[","/").replace("]","/").split("/")
    for n in range(len(textlist)):
        if n%2:
            textlist[n] = " "*len(textlist[n])
    return "/".join(textlist)

def hexcolor(tup):
    string = "#"
    for t in tup[:-1]:
        thex = hex(t)[2:]
        thex = thex if len(thex)>1 else "0"+thex
        string+=thex
    return string

def getcolor(value):
    digits = "0123456789abcdef"
    if not "(" in value:
        while not value[0] in digits:
            value = value[1:]
        end = 0
        while end<len(value) and value[end] in digits:
            end+=1
        value = value.lower()
        returned = (int(value[:2],16), int(value[2:4],16), int(value[4:6],16), 255) if end>3 else (16*int(value[0],16), 16*int(value[1],16), 16*int(value[2],16), 255)
    else:
        returned = eval(value)
    return returned

def get_layers(pixels):
    raw_pixels = []
    while "  " in pixels:
        pixels = pixels.replace("  ", " ")
    return [i for i in pixels.split(" ") if i]

def remove_codes(array, width = 0):
    limit = 1/8
    start = time()
    while True in [isinstance(i,int) for i in array] and time()-start<limit:
        num = [isinstance(i,int) for i in array].index(True)
        old = array[:]
        if array[num]==0:
            array[num] = array[num-1]
        elif array[num]==1:
            num2 = num+1
            while isinstance(array[num2],int) and array[num2]==1 and time()-start<limit:
                num2+=1
            im = Image.new("RGBA",(2,1))
            im.putdata((array[num-1],array[num2]))
            im = im.resize((num2-num+2,1), Image.BILINEAR)
            im = im.crop((1,0,im.size[0],1))
            array[num:num2+1] = list(im.getdata())
        elif array[num]==2:
            num2 = num+1
            while isinstance(array[num2],int) and time()-start<limit:
                num2+=1
            array[num:num2+1] = [array[num2]]*(num2-num+1)
        elif array[num]==3:
            variables = {"place": num}
            #print(array[num+1])
            array[num] = eval("("+array[num+1].replace("\\","/").replace("^","**")+")", variables)
            array.pop(num+1)
        elif array[num]==4:
            array[num:num+1] = (width-(num%width))*[(0,0,0,0)] if (num%width) else []
        elif array[num]==5:
            nums = [array[num-width]]
            num2 = num
            while isinstance(array[num2],int) and array[num2]==5 and time()-start<limit:
                num2+=width
            nums.append(array[num2])
            im = Image.new("RGBA",(2,1))
            im.putdata(tuple(nums))
            im = im.resize((num2-num+2,1), Image.BILINEAR)
            im = im.crop((1,0,im.size[0],1))
            buffer = list(im.getdata())
            num2 = num
            while isinstance(array[num2],int) and array[num2]==5 and time()-start<limit:
                array[num2] = buffer.pop(0)
                num2+=width
        if old!=array:
            start = time()
    while True in [isinstance(i,int) for i in array]:
        num = [isinstance(i,int) for i in array].index(True)
        array[num] = (0,0,0,0)
        
    return array

def remove_comments(text):
    return "".join(text.split("/")[::2])

def get_symbol_old(expression):
    layer = 0
    focus = 0
    if "(" in expression or "[" in expression:
        focus = min([expression.find("("),expression.find("[")])
    while focus<len(expression):
        if expression[focus]=="(" or expression[focus]=="[":
            layer+=1
        elif expression[focus]==")" or expression[focus]=="]":
            layer-=1
        focus+=1
        if not layer:
            focus-=1
            break
    print(focus,expression, expression[focus])
    return focus

def get_symbol(expression):
    return max([expression.find(")"),expression.find(")")]) if ")" in expression or "]" in expression else 0

def modefun(value, mod):
    if isinstance(value,tuple):
        new = []
        mode = sum(mod) if mod else 0
        for val in value[:-1]:
            v = val+mode
            if v<0:
                v = 0
            elif v>255:
                v = 255
            new.append(v)
        new.append(value[-1])
        return tuple(new)
    else:
        return value

def multiply(text):
    start = text.find("(")
    if start==-1:
        return text
    end = start+1
    ind = 1
    while end<len(text):
        if text[end]==")":
            ind-=1
        elif text[end]=="(":
            ind+=1
        if not ind:
            break
        else:
            end+=1
    number = end+1
    while number<len(text) and text[number] in " 0123456789*\\^%":
        number+=1
    newtext = text.replace(text[start:number],
                           text[start+1:end]*round(eval(text[text.find("*",end)+1:number])),
                           1)
    if "(" in newtext:
        return multiply(newtext)
    else:
        return newtext

def from_sgml(text):
    text = remove_comments(text.replace("\n", " "))
    if text.count("(")!=text.count(")"):
        raise SyntaxError("Invalid Parenthesis Count Error.")
    the_dict = dict([(text.split("#")[n].split(" ",1)) for n in range(1,len(text.split("#")),1)])
    if not "sgdl" in list(the_dict.keys()):
        return None
    keys = list(the_dict.keys())
    if "background" in keys:
        bg = getcolor(eval(the_dict["background"]))
    else:
        bg = (0,0,0,0)
    palette = dict([(i.split(":")[0],getcolor(i.split(":")[1])) for i in the_dict["colors"].split(" ")[:-1] if i.replace(" ","")])
    palette["_"] = (0,0,0,0)
    palette["<"] = 0
    palette["="] = 1
    palette[">"] = 2
    palette["."] = 4
    palette["|"] = 5
    
    palkeys = list(palette.keys())
    palkeys.sort(key = lambda element: -len(element))
    images = []
    if not "layers" in keys:
        pixels = the_dict["pixels"]
        raw_pixels = []
        pixels = multiply(pixels)
        modifier = eval(the_dict["modifier"]) if "modifier" in keys else 30
        mode = []
        while pixels:
            found = False
            if pixels[0]=="+":
                mode.append(modifier)
            elif pixels[0]=="-":
                mode.append(-modifier)
            if pixels[0]=="[":
                codes = ["lcopy","blur","rcopy","fun","fill","vblur"]
                code = [c in pixels[1:pixels.find("]")] for c in codes].index(True)
                raw_pixels.append(code)
                if code==3:
                    raw_pixels.append(pixels[1:pixels.find("]")].replace("fun",""))
                    #print("info",pixels[1:pixels.find("]")].replace("fun",""))
                pixels = pixels[pixels.find("]")+1:] if pixels.find("]")!=-1 else pixels[1:]
                continue
            for name in palkeys:
                if pixels.find(name)==0:
                    raw_pixels.append(modefun(palette[name],mode))
                    mode = []
                    pixels = pixels[len(name):]
                    found = True
            pixels = pixels[1:] if not found else pixels
        raw_pixels = remove_codes(raw_pixels,eval(the_dict["width"]))
        size = (eval(the_dict["width"]), ceil(len(raw_pixels)/eval(the_dict["width"])))
        image = Image.new("RGBA",size,bg)
        image.putdata(tuple(raw_pixels)[:image.size[0]*image.size[1]])
        if "scale" in keys:
            new_size = (round(eval(the_dict["scale"])*image.size[0]),round(eval(the_dict["scale"])*image.size[1]))
            image = image.resize(new_size, Image.NEAREST)
        return image
    else:
        order = get_layers(the_dict["layers"])
        for layer in order:
            pixels = the_dict[layer]
            raw_pixels = []
            pixels = multiply(pixels)
            modifier = eval(the_dict["modifier"]) if "modifier" in keys else 30
            mode = []
            while pixels:
                found = False
                if pixels[0]=="+":
                    mode.append(modifier)
                elif pixels[0]=="-":
                    mode.append(-modifier)
                if pixels[0]=="[":
                    codes = ["lcopy","blur","rcopy","fun","fill"]
                    code = [c in pixels[1:pixels.find("]")] for c in codes].index(True)
                    raw_pixels.append(code)
                    if code==3:
                        raw_pixels.append(pixels[1:pixels.find("]")].replace("fun",""))
                        #print("info",pixels[1:pixels.find("]")].replace("fun",""))
                    pixels = pixels[pixels.find("]")+1:] if pixels.find("]")!=-1 else pixels[1:]
                    continue
                for name in palkeys:
                    if pixels.find(name)==0:
                        raw_pixels.append(modefun(palette[name],mode))
                        mode = []
                        pixels = pixels[len(name):]
                        found = True
                pixels = pixels[1:] if not found else pixels
            raw_pixels = remove_codes(raw_pixels,eval(the_dict["width"]))
            size = (eval(the_dict["width"]), ceil(len(raw_pixels)/eval(the_dict["width"])))
            image = Image.new("RGBA",size,bg)
            image.putdata(tuple(raw_pixels)[:image.size[0]*image.size[1]])
            if "scale" in keys:
                new_size = (round(eval(the_dict["scale"])*image.size[0]),round(eval(the_dict["scale"])*image.size[1]))
                image = image.resize(new_size, Image.NEAREST)
            images.append(image)
        if "duration" in keys:
            x = max([im.size[0] for im in images])
            y = max([im.size[1] for im in images])
            base = Image.new("RGBA",(x,y),bg)
            for n in range(len(images)):
                im = base.copy()
                im.paste(images[n],(0,0),images[n])
                images[n] = im.copy()
            
            return images, int(the_dict["duration"])
        else:
            x = max([im.size[0] for im in images])
            y = max([im.size[1] for im in images])
            base = Image.new("RGBA",(x,y),bg)
            for layer in images:
                base.paste(layer, (0,0), layer)
            return base

def make_sgml(fnames):
    if len(fnames)==1:
        fname = fnames[0]
        image = Image.open(fname).convert("P", colors = 32, palette = Image.ADAPTIVE, dither = Image.FLOYDSTEINBERG).convert("RGBA")
        names = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM"[:32]
        colors = tuple(set(list(image.getdata())))
        pal = "#colors "
        pix = "#pixels "
        for n,color in enumerate(colors):
            col = str(color).replace(" ","")
            pal+=f"{names[n]}:{col} "
        clip = ""
        values = list(image.getdata())
        focus = 0
        while focus<len(values):
            pixel = values[focus]
            lock = names[colors.index(pixel)]
            while focus<len(values) and names[colors.index(values[focus])]==lock:
                clip+=lock
                focus+=1
            if len(clip)>3:
                pix+=f"({lock})*{len(clip)}"
            else:
                pix+=clip
            clip = ""
        if clip:
            pix+=clip

        return f"#sgdl #width {image.size[0]} {pal[:-1]} {pix[:-1]}"
    else:
        big_image = Image.new("RGBA",(Image.open(fnames[0]).size[0],Image.open(fnames[0]).size[1]*len(fnames)))
        for n, fname in enumerate(fnames):
            im = Image.open(fname).convert("RGBA")
            big_image.paste(im,(0,n*im.size[1]), im)#all_pixels.extend(list(Image.open(fname).convert("P", colors = 32, palette = Image.ADAPTIVE, dither = Image.FLOYDSTEINBERG).convert("RGBA").getdata()))
        big_image = big_image.convert("P", colors = 32, palette = Image.ADAPTIVE, dither = Image.FLOYDSTEINBERG).convert("RGBA")
        names = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM"[:32]
        colors = tuple(set(list(big_image.getdata())))

        pal = "#colors "
        for n,color in enumerate(colors):
            col = str(color).replace(" ","")
            pal+=f"{names[n]}:{col} "
        allframes = " ".join(["#layers"]+[f"frame{n}" for n in range(len(fnames))])
        framedata = []
        new_images = [big_image.crop((0,n*im.size[1],im.size[0],(n+1)*im.size[1])) for n in range(len(fnames))]
        for num, image in enumerate(new_images):
            pix = f"#frame{num} "
            clip = ""
            values = list(image.getdata())
            focus = 0
            while focus<len(values):
                pixel = values[focus]
                lock = names[colors.index(pixel)]
                while focus<len(values) and names[colors.index(values[focus])]==lock:
                    clip+=lock
                    focus+=1
                if len(clip)>3:
                    pix+=f"({lock})*{len(clip)}"
                else:
                    pix+=clip
                clip = ""
            if clip:
                pix+=clip
            framedata.append(pix)
        frametext = "\n".join(framedata)
        compiled = "\n".join(["#sgdl",
                              f"#width {image.size[0]}",
                              pal,
                              "#duration 50",
                              allframes,
                              frametext])
        return compiled

if __name__=="__main__":
    freeze_support()
    gui = GUI()
    gui.stopper_value.value = 0
